<?php

/**
 * Provides BingSiteAuth.xml functionality
 *
 * @author Johannes Richter
 */
class BingWebmaster extends Controller {


	/**
	 * Generates the response BingSiteAuth.xml content
	 *
	 * @return SS_HTTPResponse
	 */
	public function index() {
		$text = "";
		$text .= "<?xml version=\"1.0\"?> \n";
		$text .= "<users> \n";
		$text .= "	<user>" . SiteConfig::current_site_config()->BingWebmasterCode . "</user> \n";
 		$text .= "</users>";

		$response = new SS_HTTPResponse($text, 200);
		$response->addHeader("Content-Type", "text/xml; charset=\"utf-8\"");
		return $response;
	}

}

<?php

class AnalyticsExtension extends DataExtension {

	private static $db = array(
		'GoogleAnalyticsCode' => 'Varchar',
		'HotjarCode' => 'Varchar',
		'BingWebmasterCode' => 'Varchar'
	);

	public function updateCMSFields(FieldList $fields) {
		$fields->addFieldToTab('Root.Analytics', TextField::create("GoogleAnalyticsCode")->setTitle(_t('AnalyticsConfig.GOOGLE',"Google Analytics Code"))->setRightTitle("(UA-XXXXXX-X)"));
		$fields->addFieldToTab('Root.Analytics', TextField::create("HotjarCode")->setTitle(_t('AnalyticsConfig.HOTJAR',"Hotjar Code"))->setRightTitle("(XXXXXX)"));
		$fields->addFieldToTab('Root.Analytics', TextField::create("BingWebmasterCode")->setTitle(_t('AnalyticsConfig.BING',"Bing Webmaster Code")));
	}
}
